package com.pritam.android.training;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;


import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import android.widget.TextView;
import android.widget.Toast;

public class UserInputActivity extends AppCompatActivity {
    private EditText et_name, et_email,et_age,et_address,et_phone;
    private RadioGroup rg_gender;
    private ImageView profile_pic;
    private Button btn_display;
    private RadioButton rb_male,rb_female;
    private final int RESULT_LOAD_IMAGE=1;
    private final Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinput);
        linkViewtoID();

        et_phone = findViewById(R.id.phone);
        btn_display.setOnClickListener(v -> {
            String nm = et_name.getText().toString();
            String mail = et_email.getText().toString();
            String ag = et_age.getText().toString();
            String add = et_address.getText().toString();
            String ph = et_phone.getText().toString();
            if (nm.length() == 0) {

                et_name.setError("Name can't be empty");
            } else if (!nm.matches("[a-zA-Z ]+")) {

                et_name.setError("Invalid name");
            } else if (mail.length() == 0) {

                et_email.setError("Email can't be empty");
            } else if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {

                et_email.setError("Invalid email");
            }else if (ag.length() == 0) {

                et_age.setError("Age can't be empty");
            } else if (Integer.parseInt(ag)<= 0 || Integer.parseInt(ag) >= 200) {

                et_age.setError("Invalid age");
            }else if(ph.length()!=10){

                et_phone.setError("Must be of 10 digits");
            } else if (!(rb_male.isChecked() || rb_female.isChecked())) {

                Toast.makeText(UserInputActivity.this, "Please select gender", Toast.LENGTH_SHORT).show();

            }else if (add.length() == 0) {

                et_address.setError("Address can't be empty");
            } else {
                passData(nm,mail,ag,add,ph);

            }

        });
        profile_pic.setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");

            startActivityForResult(i, RESULT_LOAD_IMAGE);
        });

    }

    private void linkViewtoID() {
        et_name = findViewById(R.id.name);
        et_email = findViewById(R.id.email);
        et_age = findViewById(R.id.age);
        et_address = findViewById(R.id.address);
        btn_display = findViewById(R.id.button);
        rg_gender = findViewById(R.id.gender);
        profile_pic = findViewById(R.id.userimage);

        TextView title = findViewById(R.id.toolbar_title);
        title.setText(R.string.title_actvt_1);

        rb_male = findViewById(R.id.radio_male);
        rb_female = findViewById(R.id.radio_female);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri imageUri = data.getData();
            profile_pic.setImageURI(imageUri);
            bundle.putString("profile_image", String.valueOf(imageUri));

        }
    }

    protected void passData(String nm, String mail, String ag, String add, String ph){
        RadioButton radio = findViewById(rg_gender.getCheckedRadioButtonId());
        String gdr = radio.getText().toString();

        bundle.putString("name", nm);
        bundle.putString("email", mail);
        bundle.putString("age", ag);
        bundle.putString("gender", gdr);
        bundle.putString("address", add);
        bundle.putString("phone",ph);
        Intent intent = new Intent(UserInputActivity.this, DisplayUserDetailsActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        et_address.setText("");
        et_age.setText("");
        et_name.setText("");
        et_email.setText("");
        rg_gender.clearCheck();
    }

    @Override
    protected void onPause() {
        super.onPause();
        profile_pic.setImageResource(R.drawable.ic_user);
    }
}