package com.pritam.android.training;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;


public class DisplayUserDetailsActivity extends AppCompatActivity {
    private ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDatatoView();


        img_back.setOnClickListener(v -> {
            Intent intent = new Intent(DisplayUserDetailsActivity.this,UserInputActivity.class);
            startActivity(intent);
        });

    }

    private void setDatatoView() {
        setContentView(R.layout.activity_display_user_details);
        TextView tv_name = findViewById(R.id.name);
        TextView tv_email = findViewById(R.id.email);
        TextView tv_age = findViewById(R.id.age);
        TextView tv_gender = findViewById(R.id.gender);
        TextView tv_address = findViewById(R.id.address);
        TextView tv_phone = findViewById(R.id.phone);
        ImageView profile_img = findViewById(R.id.userimage);
        img_back = findViewById(R.id.toolbar_btn_back);
        img_back.setVisibility(View.VISIBLE);
        TextView title = findViewById(R.id.toolbar_title);
        title.setText(R.string.title_actvt_2);

        fillData(tv_name,tv_email,tv_age,tv_gender,tv_address,tv_phone,profile_img);
    }

    private void fillData(TextView tv_name, TextView tv_email, TextView tv_age,
                          TextView tv_gender, TextView tv_address, TextView tv_phone, ImageView profile_img) {
        Bundle bundle = getIntent().getExtras();
        tv_name.setText(bundle.getString("name"));
        tv_email.setText(bundle.getString("email"));
        tv_age.setText(bundle.getString("age"));
        tv_phone.setText(bundle.getString("phone"));
        tv_gender.setText(bundle.getString("gender"));
        tv_address.setText(bundle.getString("address"));
        if(bundle.containsKey("profile_image")){
            Uri imageUri = Uri.parse(bundle.getString("profile_image"));
            profile_img.setImageURI(imageUri);
        }
    }
}